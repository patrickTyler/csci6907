package edu.gwu.seas.csci6907.semester;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.deeplearning4j.models.embeddings.loader.WordVectorSerializer;
import org.deeplearning4j.models.word2vec.Word2Vec;
import org.junit.Test;
import org.nd4j.linalg.api.ndarray.INDArray;

import com.bericotech.clavin.GeoParser;
import com.bericotech.clavin.GeoParserFactory;
import com.bericotech.clavin.resolver.ResolvedLocation;

import edu.gwu.seas.csci6907.semester.TopicDateEvaluator.DateEvaluation;
import edu.gwu.seas.csci6907.semester.model.Date;
import edu.gwu.seas.csci6907.semester.model.EntityType;
import edu.gwu.seas.csci6907.semester.model.Topic;
import edu.gwu.seas.csci6907.semester.model.Date.Entity;
import edu.gwu.seas.csci6907.semester.parser.ExtractedDateParser;
import edu.gwu.seas.csci6907.semester.parser.ExtractedTopicParser;
import edu.stanford.nlp.ie.crf.CRFClassifier;
import edu.stanford.nlp.ling.CoreAnnotations;
import edu.stanford.nlp.ling.CoreLabel;

public class ExtractedDateParserTest {
	private final static String classifierFile = "english.all.3class.distsim.crf.ser.gz";
	private final static String clavinIndexDirectory = "/home/ktyler/git/CLAVIN/IndexDirectory";
	private final static String extractedDatesFile = "/home/ktyler/PycharmProjects/csci6907/time/datess.txt";
	private final static String stopList = "/home/ktyler/git/Mallet/stoplists/en.txt";
	private final static String extractedTopicFile = "/home/ktyler/Documents/strider/events/nyt_topic_analysis/6000topics.txt";
	private final static String word2vecModelFile = "/home/ktyler/git/dl4j-examples/dl4j-examples/GooyorkTimes.txt";
	private static StringBuilder vectors = new StringBuilder();
	private static int numSentencesCorrectlyPredicted = 0;
	private static int numSentencesAnalyzed = 0;
	private static int numFalsePositives = 0;
	private static int numTruePositives = 0;
	private static int numFalseNegatives = 0;
	private static int numTrueNegatives = 0;


	@Test
	public void testDateExtractor(){
		ExtractedDateParser extractedDateParser = new ExtractedDateParser();
		Map<String,List<Date>> dates = extractedDateParser
				.parseDatesFromText(
						ExtractedDateParserTest.extractedDatesFile, ExtractedDateParserTest.clavinIndexDirectory, ExtractedDateParserTest.classifierFile);

	}

	@Test
	public void testTopicExtractor(){
		ExtractedTopicParser extractedTopicParser = new ExtractedTopicParser();
		Set<String> stoplist = extractedTopicParser.buildStopList(ExtractedDateParserTest.stopList);
		List<edu.gwu.seas.csci6907.semester.model.Topic> parseTopics = extractedTopicParser.parseTopics(ExtractedDateParserTest.extractedTopicFile, stoplist);
		assert(parseTopics.get(0).getAssociatedTerms().iterator().next() != null);
	}

	//Helper functions for test cases
	private List<ResolvedLocation> getLocations(String locationInput, String indexDirectoryPath){
		GeoParser parser;
		List<ResolvedLocation> resolvedLocation = new ArrayList<>();
		StringBuilder sb = new StringBuilder();
		try {
			// Instantiate the CLAVIN GeoParser
			parser = GeoParserFactory.getDefault(indexDirectoryPath);
			//parser = GeoParserFactory.getDefault(getValueFromConfigFile("lucene"));
			// Parse location names in the text into geographic entities
			for(ResolvedLocation location : parser.parse(locationInput)){
				resolvedLocation.add(location);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return resolvedLocation;
	}
	private List<Entity> performNER(String text, String classifierFileLocation) {
		String serializedClassifier = classifierFileLocation;
		CRFClassifier<CoreLabel> classifier = 
				CRFClassifier.getClassifierNoExceptions(serializedClassifier);
		List<List<CoreLabel>> classify = classifier.classify(text);
		List<Entity> entities = new ArrayList<>();
		for (List<CoreLabel> coreLabels : classify) {
			for (CoreLabel coreLabel : coreLabels) {
				String word = coreLabel.word();
				String answer = coreLabel.get(CoreAnnotations.AnswerAnnotation.class);
				if(!"O".equals(answer)){
					if(answer.equals("ORGANIZATION")){
						entities.add(new Entity(word, EntityType.ORGANIZATION));
					} else if(answer.equals("PERSON")){
						entities.add(new Entity(word, EntityType.PERSON));
					}

				}
			}
		}
		return entities;
	}

	private void analyzeDateTopic (Date date, Topic topic, double [] weightVector, Word2Vec word2vec,
			Set<String> stoplist, Set<String> commonFalsePositives, boolean isRelated){
		date.setLocations(getLocations(date.getSentence(), ExtractedDateParserTest.clavinIndexDirectory));
		date.setEntities(performNER(date.getSentence(), ExtractedDateParserTest.classifierFile));
		DateEvaluation evaluation = TopicDateEvaluator.estimateConfidence(topic, date, ExtractedDateParserTest.clavinIndexDirectory, 
				word2vec, stoplist, commonFalsePositives, weightVector);
		double confidenceScore = evaluation.getConfidenceScore();
		INDArray confidenceVector = evaluation.getConfidenceVector();
		vectors.append(confidenceVector.getDouble(0, 0));
		vectors.append(",");
		vectors.append(confidenceVector.getDouble(0, 1));
		vectors.append(",");
		vectors.append(confidenceVector.getDouble(0, 2));
		vectors.append(",");
		vectors.append(confidenceVector.getDouble(0, 3));
		vectors.append(",");
		vectors.append(confidenceVector.getDouble(0, 4));
		vectors.append(",");
		if(isRelated){
			vectors.append("1\n");
		} else {
			vectors.append("0\n");
		}
		System.out.print("Date #" + ++ExtractedDateParserTest.numSentencesAnalyzed + " evaluation: " + 
				confidenceScore);
		if((confidenceScore >= .65 && isRelated) || (!isRelated && confidenceScore < .65)){
			System.out.print(" was predicted correctly.\n");
			if(confidenceScore >= .65 && isRelated){
				ExtractedDateParserTest.numTruePositives++;
			}
			if((!isRelated) && confidenceScore < .65){
				ExtractedDateParserTest.numTrueNegatives++;
			}
			ExtractedDateParserTest.numSentencesCorrectlyPredicted++;
		} else{
			System.out.print(" was predicted incorrectly.\n");
			if(isRelated && confidenceScore < .65 ){
				ExtractedDateParserTest.numFalseNegatives++;
			}
			if((!isRelated) && confidenceScore >= .65){
				ExtractedDateParserTest.numFalsePositives++;
			}
		}
	}

	@Test
	public void testDateComparison(){
		//Common false positives
		Set<String> commonFalsePositives = new HashSet<String>() {{
			add("anniversary");
			add("commemoration");
			add("commemorate");
			add("trial");
			add("court");
			add("celebration");
			add("reunion");
			add("after");
			add("remember");
			add("mourning");
			add("years ago");
		}};

		//Word2Vec model
		Word2Vec word2vec = WordVectorSerializer.readWord2VecModel(ExtractedDateParserTest.word2vecModelFile);

		//Stop words
		ExtractedTopicParser extractedTopicParser = new ExtractedTopicParser();
		Set<String> stoplist = extractedTopicParser.buildStopList(ExtractedDateParserTest.stopList);

		//Weight vector, where indices 0,1,2,3,4 correspond to
		//	locational similarity, entity similarity, path score,
		//	embedding similarity, and false positives words score respectively.
		//	
		//	Note that these should sum to 1.0.
		double [] weightVector = {0.2, 0.2, 0.2, 0.2, 0.2};

		//New Orleans topic
		Topic newOrleansTopic = new Topic("new orleans hurricane katrina 2005 corps flood levy engineers african lousiana august");
		//Date 1
		Date date = new Date(0, 0, "August 2005", "", "");
		date.setSentence("The Army Corps of Engineers were critcized during the "
				+ "flood of Hurrican Katrina, which occured in New Orleans, Louisiana in August 2005.");

		analyzeDateTopic(date, newOrleansTopic, weightVector, word2vec,
				stoplist, commonFalsePositives, true);
		//Date 2
		Date date2 = new Date(0, 0, "August 2005", "", "");
		date2.setSentence("Today marks the ten-year anniversary of Hurrican Katrina, which occured in New Orleans, Louisiana in August 2005.");
		analyzeDateTopic(date2, newOrleansTopic, weightVector, word2vec,
				stoplist, commonFalsePositives, false);

		//Date 3
		Date date3 = new Date(0, 0, "Saturdays", "", "");
		date3.setSentence("The results of Saturdays mayoral primary, four-and-a-half years after Hurricane Katrina violently jarred "
				+ "New Orleans from its trajectory, will say much about how this city views race, how it views leadership and how it views "
				+ "its challenges in the immediate future.");
		analyzeDateTopic(date3, newOrleansTopic, weightVector, word2vec,
				stoplist, commonFalsePositives, false);


		//Date 4
		Date date4 = new Date(0, 0, "last years", "", "");
		date4.setSentence("There is simply no place for them to go in New Orleans, said Walter L. Jones, director of community-based "
				+ "initiatives for Neighborhood Centers of Houston, which has worked with about 2,200 families displaced by last years hurricanes.");
		analyzeDateTopic(date4, newOrleansTopic, weightVector, word2vec,
				stoplist, commonFalsePositives, true);

		//Date 5
		Date date5 = new Date(0, 0, "last year", "", "");
		date5.setSentence("This weeks arrest of a doctor and two nurses in connection with the deaths of four elderly hospital patients "
				+ "during Hurricane Katrinas flooding last year served as a reminder of the storms continuing toll on the citys oldest and poorest residents.");
		analyzeDateTopic(date5, newOrleansTopic, weightVector, word2vec,
				stoplist, commonFalsePositives, true);

		//Date 6
		Date date6 = new Date(0, 0, "Monday", "", "");
		date6.setSentence("Mr. Bush has prodded Congress to approve tens of billions of dollars for rebuilding and victim assistance, delivered a much-publicized"
				+ " fence-mending speech to the N.A.A.C.P.  and made repeated trips to the Gulf Coast, where he plans to observe the anniversary of the "
				+ "storm Monday and Tuesday.");
		analyzeDateTopic(date6, newOrleansTopic, weightVector, word2vec,
				stoplist, commonFalsePositives, false);

		//Date 7
		Date date7 = new Date(0, 0, "Saturday", "", "");
		date7.setSentence("A wreath commemorating victims of Hurricane Katrina on Saturday, the fourth anniversary of the storm.");
		analyzeDateTopic(date7, newOrleansTopic, weightVector, word2vec,
				stoplist, commonFalsePositives, false);

		//Date 8
		Date date8 = new Date(0, 0, "four days before", "", "");
		date8.setSentence("It was a day before the third anniversary of Katrinas arrival here, and four days before Govenor "
				+ "Bobby Jindal of Louisiana said tropical-force winds might start hitting his states coast once again.");
		analyzeDateTopic(date8, newOrleansTopic, weightVector, word2vec,
				stoplist, commonFalsePositives, false);

		//Date 9
		Date date9 = new Date(0, 0, "Tuesday evening", "", "");
		date9.setSentence("The prime-time network television coverage of the convention opened on Tuesday evening competing with "
				+ "images of driving rain and powerful winds in Louisiana during a week that marks the seventh "
				+ "anniversary of Hurricane Katrina.");
		analyzeDateTopic(date9, newOrleansTopic, weightVector, word2vec,
				stoplist, commonFalsePositives, false);

		//Date 10
		Date date10 = new Date(0, 0, "August 2005", "", "");
		date10.setSentence("He was called back to active duty with only a few minutes notice in August 2005, when Hurricane Katrina "
				+ "hit and he was sent to a St. Louis command center to work in the response operation.");
		analyzeDateTopic(date10, newOrleansTopic, weightVector, word2vec,
				stoplist, commonFalsePositives, true);




		//Bosnian Topic
		Topic bosnianTopic = new Topic("karadzic bosnian popovic genocide ruez srebrenica radovan serb beara jean-ren war-crimes "
				+ "zdravko karadzics hague vujadin popovics pacifists builds lessened sehemerovic");
		//Date 11
		Date date11 = new Date(0, 0, "Monday", "", "");
		date11.setSentence("The International Court of Justice on Monday for the first time called the massacre of Bosnian Muslims "
				+ "at Srebrenica in 1995 an act of genocide, but determined that Serbia itself was not guilty of the enormous crime.");
		analyzeDateTopic(date11, bosnianTopic, weightVector, word2vec,
				stoplist, commonFalsePositives, false);

		//Date 12
		Date date12 = new Date(0, 0, "1992 to 1995", "", "");
		date12.setSentence("During the war in Bosnia, from 1992 to 1995, the United Nations declared Srebrenica "
				+ "a haven and promised to protect it.");
		analyzeDateTopic(date12, bosnianTopic, weightVector, word2vec,
				stoplist, commonFalsePositives, true);

		//Date 13
		Date date13 = new Date(0, 0, "last year", "", "");
		date13.setSentence("Although Mr. Milosevic died last year before the end of his four-year trial, "
				+ "other senior Serbian political and military leaders are still facing trial at the tribunal f"
				+ "or the former Yugoslavia.");
		analyzeDateTopic(date13, bosnianTopic, weightVector, word2vec,
				stoplist, commonFalsePositives, false);

		//Date 14
		Date date14 = new Date(0, 0, "July 1995", "", "");
		date14.setSentence("Nonetheless, it faulted Serbia, saying it could and should have prevented the genocide and, "
				+ "in its aftermath, should have punished the Bosnian Serbs who systematically killed close to 8,000 men "
				+ "and boys in July 1995.");
		analyzeDateTopic(date14, bosnianTopic, weightVector, word2vec,
				stoplist, commonFalsePositives, true);

		//Date 15
		Date date15 = new Date(0, 0, "Thursday", "", "");
		date15.setSentence("Until Thursday, that is, when the village was briefly overrun by reporters and photographers "
				+ "reacting to the news from Belgrade that the authorities had found and arrested Mr. Mladic in Lazarevo.");
		analyzeDateTopic(date15, bosnianTopic, weightVector, word2vec,
				stoplist, commonFalsePositives, true);
		//Date 16
		Date date16 = new Date(0, 0, "1995", "", "");
		date16.setSentence(" But Lazarevos feeling of loss at Mr. Mladics arrest is probably linked to the shift in "
				+ "Serbias political tides since he was first indicted in The Hague in 1995.");
		analyzeDateTopic(date16, bosnianTopic, weightVector, word2vec,
				stoplist, commonFalsePositives, true);

		//Date 17
		Date date17 = new Date(0, 0, "Tuesday", "", "");
		date17.setSentence("After four years of hearings and hundreds of witnesses, the war crimes trial of Radovan "
				+ "Karadzic, the former Bosnian Serb leader, ended on Tuesday, with Mr. Karadzic proclaiming his "
				+ "innocence and the prosecution demanding that he be convicted and given the "
				+ "harshest sentence: life imprisonment.");
		analyzeDateTopic(date17, bosnianTopic, weightVector, word2vec,
				stoplist, commonFalsePositives, false);

		//Date 18
		Date date18 = new Date(0, 0, "1992-95", "", "");
		date18.setSentence(" Last week, Mr. Robinson spoke in Mr. Karadzics defense on the gravest "
				+ "charges  two counts of genocide  saying that he had neither knowledge of nor intent "
				+ "to commit the mass murders of Bosnias Croats and Muslims during the 1992-95 war.");
		analyzeDateTopic(date18, bosnianTopic, weightVector, word2vec,
				stoplist, commonFalsePositives, true);

		//Date 19
		Date date19 = new Date(0, 0, "July 1995", "", "");
		date19.setSentence("But the single crime that has provoked the greatest international "
				+ "outrage involves the execution of more than 7,000 men and boys in July 1995 in "
				+ "and around the eastern town of Srebrenica, which had been declared a United Nations "
				+ "safe area two years earlier.");
		analyzeDateTopic(date19, bosnianTopic, weightVector, word2vec,
				stoplist, commonFalsePositives, true);

		//Date 20
		Date date20 = new Date(0, 0, "1992-95", "", "");
		date20.setSentence("Bosnian families on Sunday buried the remains of 283 Muslims and one "
				+ "Roman Catholic Croat who were killed at the start of the Bosnian war of 1992-95.");
		analyzeDateTopic(date20, bosnianTopic, weightVector, word2vec,
				stoplist, commonFalsePositives, true);



		//American Intel/Drone strikes topic
		Topic americanIntelTopic = new Topic("american intelligence officials states united qaeda security "
				+ "c.i.a counterterrorism terrorist drone attacks terrorism official obama "
				+ "yemen strikes threat administration washington");

		//Date 21
		Date date21 = new Date(0, 0, "Wednesday", "", "");
		date21.setSentence("Yemeni security officials said Wednesday that they had foiled an audacious plot "
				+ "by Al Qaeda to seize an important port and kidnap or kill foreigners working there, but "
				+ "the claim aroused some skepticism among Yemenis and independent terrorism analysts.");
		analyzeDateTopic(date21, americanIntelTopic, weightVector, word2vec,
				stoplist, commonFalsePositives, true);

		//Date 22
		Date date22 = new Date(0, 0, "Thursday", "", "");
		date22.setSentence("Hours later, three drone strikes early Thursday east of the capital killed at least three people, according to witnesses.");
		analyzeDateTopic(date22, americanIntelTopic, weightVector, word2vec,
				stoplist, commonFalsePositives, true);

		//Date 23
		Date date23 = new Date(0, 0, "Thursdays", "", "");
		date23.setSentence("Thursdays drone strikes hit one of two cars carrying Qaeda members in the Erq Al Shabwan area of the Abeidah Valley in Mareb.");
		analyzeDateTopic(date23, americanIntelTopic, weightVector, word2vec,
				stoplist, commonFalsePositives, true);

		//Date 24
		Date date24 = new Date(0, 0, "Friday", "", "");
		date24.setSentence("But on Friday, a three-judge panel of the court unanimously reversed his ruling, saying the "
				+ "judiciary must defer to the governments assertions that, for security reasons, guards needed to conduct "
				+ "more thorough searches; under the old procedures, guards shook inmates underwear to dislodge any contraband "
				+ "but did not touch their buttocks or genitals.");
		analyzeDateTopic(date24, americanIntelTopic, weightVector, word2vec,
				stoplist, commonFalsePositives, false);

		//Date 25
		Date date25 = new Date(0, 0, "Mondays", "", "");
		date25.setSentence("Mondays memorandum concluded that such a case would not be a war crime, "
				+ "although the operator might be in theoretical jeopardy of being prosecuted in a "
				+ "Yemeni court for violating Yemens domestic laws against murder, a highly unlikely possibility.");
		analyzeDateTopic(date25, americanIntelTopic, weightVector, word2vec,
				stoplist, commonFalsePositives, false);
		//Date 26
		Date date26 = new Date(0, 0, "Sept. 30, 2011", "", "");
		date26.setSentence("In other words, as Dzhokhar told investigators, they followed the script from Inspire magazine, "
				+ "which Mr. Khan published in Yemen along with his mentor, the cleric Anwar al-Awlaki, "
				+ "who was killed in the same drone strike on Sept. 30, 2011.");
		analyzeDateTopic(date26, americanIntelTopic, weightVector, word2vec,
				stoplist, commonFalsePositives, true);

		//Date 27
		Date date27 = new Date(0, 0, "Friday", "", "");
		date27.setSentence("His life ended in Yemen on Friday, when Mr. Khan, 25, was killed in a drone strike that "
				+ "also took the life of the radical cleric Anwar al-Awlaki and two other men, according to both "
				+ "American and Yemeni officials.");
		analyzeDateTopic(date27, americanIntelTopic, weightVector, word2vec,
				stoplist, commonFalsePositives, true);

		//Date 28
		Date date28 = new Date(0, 0, "Wednesday", "", "");
		date28.setSentence("His warning on Wednesday about challenges to come still hung in the air as he observed the "
				+ "anniversary of battles past and expressed hope that the United States would remain resilient in the face of terrorist threats.");
		analyzeDateTopic(date28, americanIntelTopic, weightVector, word2vec,
				stoplist, commonFalsePositives, false);

		//Date 29
		Date date29 = new Date(0, 0, "Sept. 11", "", "");
		date29.setSentence("Reliant on Mr. Gates, Mr. Obama has made limited efforts to know his service chiefs or top commanders, "
				+ "and has visited the Pentagon only once, not counting a Sept. 11 commemoration.");
		analyzeDateTopic(date29, americanIntelTopic, weightVector, word2vec,
				stoplist, commonFalsePositives, false);

		//Date 30
		Date date30 = new Date(0, 0, "November", "", "");
		date30.setSentence("The Pakistani Parliament demanded an unconditional apology for the November attack and an immediate end "
				+ "to the C.I.A. drone strikes, but it also paved the way for a reopening of NATO supply lines through Pakistan, "
				+ "though at a cost that the administration and members of Congress viewed as extortion.");
		analyzeDateTopic(date30, americanIntelTopic, weightVector, word2vec,
				stoplist, commonFalsePositives, true);




		//FBI Topic
		Topic fbiTopic = new Topic("f.b.i comey mueller bureau f.b.i.s agents bureaus muellers iii comeys spencer "
				+ "nelson gonzales morris ashcroft freeh edgar hoover mudd fitzgerald");

		//Date 31
		Date date31 = new Date(0, 0, "next week", "", "");
		date31.setSentence("James B. Comey will begin shadowing the F.B.I. director, Robert S. Mueller "
				+ "III, next week as he prepares to take on the job himself.");
		analyzeDateTopic(date31, fbiTopic, weightVector, word2vec,
				stoplist, commonFalsePositives, true);

		//Date 32
		Date date32 = new Date(0, 0, "Sept. 5", "", "");
		date32.setSentence("But his off-the-job training stops Sept. 5, when Mr. "
				+ "Comey will take over from Mr. Mueller and begin a 10-year term.");
		analyzeDateTopic(date32, fbiTopic, weightVector, word2vec,
				stoplist, commonFalsePositives, true);

		//Date 33
		Date date33 = new Date(0, 0, "Sept. 11, 2012", "", "");
		date33.setSentence("One of the most politically contentious issues Mr. "
				+ "Comey will face, current and former bureau officials say, is the continuing "
				+ "investigation into the attacks on the United States mission in Benghazi, Libya, "
				+ "on Sept. 11, 2012, that killed Ambassador J. Christopher Stevens.");
		analyzeDateTopic(date33, fbiTopic, weightVector, word2vec,
				stoplist, commonFalsePositives, true);

		//Date 34
		Date date34 = new Date(0, 0, "today", "", "");
		date34.setSentence("The 13th anniversary of the 9/11 attacks "
				+ "was observed today at the World Trade Center in Lower Manhattan.");
		analyzeDateTopic(date34, fbiTopic, weightVector, word2vec,
				stoplist, commonFalsePositives, false);

		//Date 35
		Date date35 = new Date(0, 0, "Sept. 11", "", "");
		date35.setSentence("At least two prominent figures are said to have made it clear that they do "
				+ "not want to be considered for the job, including Merrick B. Garland, a federal appeals "
				+ "court judge, and Jamie Gorelick, a former deputy attorney general and a member of the Sept. 11 commission.");
		analyzeDateTopic(date35, fbiTopic, weightVector, word2vec,
				stoplist, commonFalsePositives, false);
		//Date 36
		Date date36 = new Date(0, 0, "1996", "", "");
		date36.setSentence("While Mr. Comey was working in Richmond, Mr. Ashcroft "
				+ "asked him in 2001 to take over the governments foundering investigation of the "
				+ "1996 terrorist bombing at Khobar Towers in Saudi Arabia, which killed 19 American service members.");
		analyzeDateTopic(date36, fbiTopic, weightVector, word2vec,
				stoplist, commonFalsePositives, true);

		//Date 37
		Date date37 = new Date(0, 0, "Thursday", "", "");
		date37.setSentence("In granting him asylum on Thursday, President Rafael Correa of Ecuador presented his move as a "
				+ "pre-emptive action against American plans to seek Mr. Assanges extradition and put him on trial in the "
				+ "United States on espionage charges for his role in publishing American military and diplomatic documents.");
		analyzeDateTopic(date37, fbiTopic, weightVector, word2vec,
				stoplist, commonFalsePositives, false);

		//Date 38
		Date date38 = new Date(0, 0, "Feb. 14", "", "");
		date38.setSentence("The notes in the journal that F.B.I. agents found in his apartment on Feb. 14 stood in stark "
				+ "contrast to many of the postings he put on a blog during his first year as a student in America, "
				+ "when he studied English at Vanderbilt University in Nashville. ");
		analyzeDateTopic(date38, fbiTopic, weightVector, word2vec,
				stoplist, commonFalsePositives, true);

		//Date 39
		Date date39 = new Date(0, 0, "2002", "", "");
		date39.setSentence("Mr. Abu Ghaith went to Iran in 2002, one of a few Qaeda operatives who traveled there in the months after the Sept. 11 attacks.");
		analyzeDateTopic(date39, fbiTopic, weightVector, word2vec,
				stoplist, commonFalsePositives, true);

		//Date 40
		Date date40 = new Date(0, 0, "1989", "", "");
		date40.setSentence("Federal prosecutors in Columbus said the suspect, Christopher Paul, 43, began supporting terrorists in the United States, "
				+ "Africa and Europe in 1989, buying explosives and helping to train radical Islamic fundamentalists in Germany.");
		analyzeDateTopic(date40, fbiTopic, weightVector, word2vec,
				stoplist, commonFalsePositives, true);




		//Malawi Topic
		Topic malawiTopic = new Topic("malawi mutharika banda malawis chimbalanga drennan bingu blantyre peter "
				+ "joyce lilongwe monjeza mutharikas malawian mwaraya tiwo tiwonge malawi's malawians kaliati");

		//Date 41
		Date date41 = new Date(0, 0, "Tuesday", "", "");
		date41.setSentence("In a sharp blow to the international standing of Malawi, an agency of the "
				+ "American government on Tuesday froze a $350 million grant to the nation after "
				+ "antigovernment protests there last week left 19 people dead.");
		analyzeDateTopic(date41, malawiTopic, weightVector, word2vec,
				stoplist, commonFalsePositives, true);

		//Date 42
		Date date42 = new Date(0, 0, "Saturday", "", "");
		date42.setSentence("Malawis vice president, Joyce Banda, was sworn in as president on "
				+ "Saturday, ending a tense 36 hours of speculation and confusion about the "
				+ "future of one of central Africas most enduring democracies after the death of President Bingu wa Mutharika on Thursday.");
		analyzeDateTopic(date42, malawiTopic, weightVector, word2vec,
				stoplist, commonFalsePositives, true);

		//Date 43
		Date date43 = new Date(0, 0, "Thursday", "", "");
		date43.setSentence("Mr. Mutharika, 78, had a heart attack on Thursday, but the government refused to confirm his death until Saturday.");
		analyzeDateTopic(date43, malawiTopic, weightVector, word2vec,
				stoplist, commonFalsePositives, true);

		//Date 44
		Date date44 = new Date(0, 0, "one year", "", "");
		date44.setSentence("President Bingu wa Mutharika of Malawi, speaking on behalf of the African Union, "
				+ "urged the General Assembly to defer for one year the war crimes charges brought by the International "
				+ "Criminal Court against President Omar Hassan al-Bashir of Sudan.");
		analyzeDateTopic(date44, malawiTopic, weightVector, word2vec,
				stoplist, commonFalsePositives, false);

		//Date 45
		Date date45 = new Date(0, 0, "two years ago", "", "");
		date45.setSentence("Ms. Banda enjoyed huge good will when she came to power two years ago, but her popularity"
				+ " waned after she was forced to impose austerity measures, and after a scandal in which large sums"
				+ " of cash were found in the car of a senior government official.");
		analyzeDateTopic(date45, malawiTopic, weightVector, word2vec,
				stoplist, commonFalsePositives, false);
		//Date 46
		Date date46 = new Date(0, 0, "Jan. 11", "", "");
		date46.setSentence("The trial of Mr. Chimbalanga and Mr. Monjeza began on Jan. 11, with "
				+ "hundreds gathering outside the decrepit courtroom, hooting and jeering.");
		analyzeDateTopic(date46, malawiTopic, weightVector, word2vec,
				stoplist, commonFalsePositives, false);

		//Date 47
		Date date47 = new Date(0, 0, "Tuesdays", "", "");
		date47.setSentence("Magistrate Nyakwawa Usiwa Usiwa, in delivering Tuesdays judgment in a small "
				+ "courtroom in Blantyre, the countrys commercial capital, was similarly stern.");
		analyzeDateTopic(date47, malawiTopic, weightVector, word2vec,
				stoplist, commonFalsePositives, true);

		//Date 48
		Date date48 = new Date(0, 0, "Three days later", "", "");
		date48.setSentence("Three days later, Mr. Chimbalanga arrived in court noticeably ill.");
		analyzeDateTopic(date48, malawiTopic, weightVector, word2vec,
				stoplist, commonFalsePositives, false);

		//Date 49
		Date date49 = new Date(0, 0, "10 days", "", "");
		date49.setSentence("Earlier, Mrs. Banda declared 10 days of mourning, and ambassadors, "
				+ "the army chief, government officials and other prominent figures have been streaming to her home in Lilongwe.");
		analyzeDateTopic(date49, malawiTopic, weightVector, word2vec,
				stoplist, commonFalsePositives, false);

		//Date 50
		Date date50 = new Date(0, 0, "", "", "");
		date50.setSentence("On Friday, the information minister, Patricia Kaliati, hinted that the presidents party, "
				+ "the Democratic Progressive Party, did not see Mrs. Banda as a legitimate successor because "
				+ "she had been expelled from the governing party.");
		analyzeDateTopic(date50, malawiTopic, weightVector, word2vec,
				stoplist, commonFalsePositives, true);




		//Germany topic
		Topic germanyTopic = new Topic("merkel germany german mrs chancellor berlin angela democrats merkels germanys "
				+ "christian social germans free coalition union wulff east state european");

		//Date 51
		Date date51 = new Date(0, 0, "Friday", "", "");
		date51.setSentence("The German agriculture minister, Hans-Peter Friedrich, resigned on "
				+ "Friday after it was revealed that four months ago, as interior minister, "
				+ "he tipped off another senior politician to the criminal investigation of a prominent lawmaker.");
		analyzeDateTopic(date51, germanyTopic, weightVector, word2vec,
				stoplist, commonFalsePositives, true);

		//Date 52
		Date date52 = new Date(0, 0, "March", "", "");
		date52.setSentence("Such an outcome, said Demetris Syllouris, a member of the Cypriot Parliament"
				+ " and president of the European Party, a once enthusiastic pro-Europe political group, "
				+ "is exactly the opposite of what European leaders, particularly Angela Merkel of Germany, "
				+ "wanted when they set out to cleanse Cypruss banking system in March.");
		analyzeDateTopic(date52, germanyTopic, weightVector, word2vec,
				stoplist, commonFalsePositives, true);

		//Date 53
		Date date53 = new Date(0, 0, "months ago", "", "");
		date53.setSentence("Instead, an individual court order would have to be obtained  a far slower process "
				+ "that, just months ago, Mr. Obamas intelligence team insisted would be too cumbersome in halting attacks.");
		analyzeDateTopic(date53, germanyTopic, weightVector, word2vec,
				stoplist, commonFalsePositives, false);

		//Date 54
		Date date54 = new Date(0, 0, "recently", "", "");
		date54.setSentence(" France and Germany recently issued a joint postage stamp as part of a yearlong "
				+ "celebration of the 50th anniversary of the lyse Treaty, the landmark agreement between the two former enemies.");
		analyzeDateTopic(date54, germanyTopic, weightVector, word2vec,
				stoplist, commonFalsePositives, false);

		//Date 55
		Date date55 = new Date(0, 0, "Tuesdays", "", "");
		date55.setSentence("In a year loaded with symbolic gestures and 4,000 commemorative events, including Tuesdays joint session of "
				+ "Parliament, joint cabinet dinner and a concert, that 5-cent disparity is a reminder that despite "
				+ "the decades of friendship and enormous day-to-day cooperation, significant, often devilish, differences persist.");
		analyzeDateTopic(date55, germanyTopic, weightVector, word2vec,
				stoplist, commonFalsePositives, false);
		//Date 56
		Date date56 = new Date(0, 0, "20th year", "", "");
		date56.setSentence("This 20th year since the fall of the Berlin Wall has been packed with ritualized "
				+ "public commemorations, some of which were likened here to the gala anniversary celebrations of Communist times.");
		analyzeDateTopic(date56, germanyTopic, weightVector, word2vec,
				stoplist, commonFalsePositives, false);

		//Date 57
		Date date57 = new Date(0, 0, "70th anniversary", "", "");
		date57.setSentence("Mr. Putin used a trip to France commemorating the 70th anniversary of D-Day to meet "
				+ "for the first time with his newly elected counterpart in Ukraine, "
				+ "Petro O. Poroshenko, and the two discussed the possibility of a "
				+ "cease-fire in the eastern part of the country.");
		analyzeDateTopic(date57, germanyTopic, weightVector, word2vec,
				stoplist, commonFalsePositives, false);

		//Date 58
		Date date58 = new Date(0, 0, "Friday", "", "");
		date58.setSentence("Ukraine's President-elect Petro Poroshenko passed Russian President "
				+ "Vladimir V. Putin at the commemoration of the 70th anniversary of the D-Day in Ouistreham, France, on Friday.");
		analyzeDateTopic(date58, germanyTopic, weightVector, word2vec,
				stoplist, commonFalsePositives, false);

		//Date 59
		Date date59 = new Date(0, 0, "Wednesday", "", "");
		date59.setSentence("Addressing Germanys Parliament on Wednesday, Chancellor Angela Merkel used stark terms to describe the fighting.");
		analyzeDateTopic(date59, germanyTopic, weightVector, word2vec,
				stoplist, commonFalsePositives, true);

		//Date 60
		Date date60 = new Date(0, 0, "Septembers", "", "");
		date60.setSentence("Chancellor Angela Merkel, a decisive winner in Septembers elections but muted ever since about where she plans to "
				+ "take Europes biggest economy, embarks this week on a third term that could put her stamp on German and European history, "
				+ "but only, critics say, if she abandons her cautious tactics for a more strategic embrace of the demands of the 21st century.");
		analyzeDateTopic(date60, germanyTopic, weightVector, word2vec,
				stoplist, commonFalsePositives, true);



		//Jamaica Topic
		Topic jamaicaTopic = new Topic("jamaica kingston caribbean coke trinidad jamaican gardens tivoli cokes "
				+ "woolmer dons jamaicas islands golding jamaicans tobago christopher grenada don simpson-miller");
		//Date 61
		Date date61 = new Date(0, 0, "Firday", "", "");
		date61.setSentence(" When she arrived in court on Friday, Ms. Dookhan, 35, "
				+ "first showed little emotion, staring straight ahead with her face set.");
		analyzeDateTopic(date61, jamaicaTopic, weightVector, word2vec,
				stoplist, commonFalsePositives, false);

		//Date 62
		Date date62 = new Date(0, 0, "2002", "", "");
		date62.setSentence("The couple married in 2002 and moved back to Aylesbury in 2003.");
		analyzeDateTopic(date62, jamaicaTopic, weightVector, word2vec,
				stoplist, commonFalsePositives, true);

		//Date 63
		Date date63 = new Date(0, 0, "2010", "", "");
		date63.setSentence("Assistance to some Jamaican security forces, for example, has been "
				+ "suspended while the State Department examines human rights allegations stemming from an "
				+ "operation in 2010 to arrest the drug lord Christopher Coke, whom the United States had requested for extradition.");
		analyzeDateTopic(date63, jamaicaTopic, weightVector, word2vec,
				stoplist, commonFalsePositives, true);

		//Date 64
		Date date64 = new Date(0, 0, "today", "", "");
		date64.setSentence("A United States citizen flying home today from a ski jaunt in Canada, a beach break in "
				+ "Mexico or a honeymoon in Jamaica"
				+ " can flash a drivers license or a birth certificate at airport customs officials and walk on through.");
		analyzeDateTopic(date64, jamaicaTopic, weightVector, word2vec,
				stoplist, commonFalsePositives, true);

		//Date 65
		Date date65 = new Date(0, 0, "February", "", "");
		date65.setSentence("The change comes at another important juncture in the modern monarchy, "
				+ "the 60th anniversary in February of the succession of Queen Elizabeth II to the throne.");
		analyzeDateTopic(date65, jamaicaTopic, weightVector, word2vec,
				stoplist, commonFalsePositives, false);
		//Date 66
		Date date66 = new Date(0, 0, "Wednesday", "", "");
		date66.setSentence("The convictions were the first in the case, and they seemed likely to lead to lengthy "
				+ "jail terms for the two men, who were juveniles at the time of the attack, when the court sits again on Wednesday.");
		analyzeDateTopic(date66, jamaicaTopic, weightVector, word2vec,
				stoplist, commonFalsePositives, false);

		//Date 67
		Date date67 = new Date(0, 0, "Aug 20", "", "");
		date67.setSentence("Aug 20 Hurricane Dean buffeted Jamaica's southern coast, flooding "
				+ "the capital and littering it with broken trees and roofs after killing nine people "
				+ "earlier on its run through the Caribbean.");
		analyzeDateTopic(date67, jamaicaTopic, weightVector, word2vec,
				stoplist, commonFalsePositives, true);

		//Date 68
		Date date68 = new Date(0, 0, "May 12, 1926", "", "");
		date68.setSentence("Mervyn Malcolm Dymally was born May 12, 1926, in Bonasse Village in Cedros, Trinidad.");
		analyzeDateTopic(date68, jamaicaTopic, weightVector, word2vec,
				stoplist, commonFalsePositives, true);

		//Date 69
		Date date69 = new Date(0, 0, "1983", "", "");
		date69.setSentence("Behind the legislation were concerns over divided military command authority a"
				+ "rising from the 1983 invasion of Grenada and the failed hostage-rescue mission.");
		analyzeDateTopic(date69, jamaicaTopic, weightVector, word2vec,
				stoplist, commonFalsePositives, true);

		//Date 70
		Date date70 = new Date(0, 0, "", "", "");
		date70.setSentence(" Leaving the Army for the Central Intelligence Agency, he joined the invasion of Grenada, "
				+ "and after the Marine barracks in Beirut were bombed in 1983, killing 241 United States "
				+ "servicemen, he was given sensitive counterterrorism work in Lebanon.");
		analyzeDateTopic(date70, jamaicaTopic, weightVector, word2vec,
				stoplist, commonFalsePositives, true);

		System.out.println("Total precision: " + ((ExtractedDateParserTest.numSentencesCorrectlyPredicted * 1.0) / ExtractedDateParserTest.numSentencesAnalyzed)
				+ ", number of sentences analyzed: " + ExtractedDateParserTest.numSentencesAnalyzed + ", number of sentences correctly predicted: " + 
				ExtractedDateParserTest.numSentencesCorrectlyPredicted);
		System.out.println("True positives: " + ExtractedDateParserTest.numTruePositives + ", true negatives: " + ExtractedDateParserTest.numTrueNegatives
				+ ", false positives: " + ExtractedDateParserTest.numFalsePositives + ", false negatives: " + ExtractedDateParserTest.numFalseNegatives);
		
		//Write report to CSV
		writeVectorsToCSV();

	}
	
	private void writeVectorsToCSV(){
		BufferedWriter bw = null;
		try {
			bw = new BufferedWriter(new FileWriter("/home/ktyler/workspace/csci6907/src/test/resources/dateVectors.csv"));
			bw.write(vectors.toString());
		} catch (IOException e) {
			   e.printStackTrace();
		}
		finally
		{ 
		   try{
		      if(bw!=null)
			 bw.close();
		   }catch(Exception e){
		       System.out.println("Error in closing the BufferedWriter"+e);
		    }
		}
	}
}	
