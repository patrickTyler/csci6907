package edu.gwu.seas.csci6907.semester;

import java.io.IOException;
import java.util.Arrays;

import org.datavec.api.records.reader.RecordReader;
import org.datavec.api.records.reader.impl.csv.CSVRecordReader;
import org.datavec.api.split.FileSplit;
import org.datavec.api.util.ClassPathResource;
import org.deeplearning4j.datasets.datavec.RecordReaderDataSetIterator;
import org.nd4j.linalg.api.ndarray.INDArray;
import org.nd4j.linalg.dataset.DataSet;
import org.nd4j.linalg.dataset.SplitTestAndTrain;
import org.nd4j.linalg.dataset.api.iterator.DataSetIterator;
import org.nd4j.linalg.dataset.api.preprocessor.DataNormalization;
import org.nd4j.linalg.dataset.api.preprocessor.NormalizerStandardize;
import org.nd4j.linalg.factory.Nd4j;

import edu.gwu.seas.csci6907.semester.TopicDateNeural.Data;

public class TopicDateLogistic {
	
	private Data preprocessData(String vectors){
		//First: get the dataset using the record reader. CSVRecordReader handles loading/parsing
        int numLinesToSkip = 0;
        
        String delimiter = ",";
        RecordReader recordReader = new CSVRecordReader(numLinesToSkip,delimiter);
        try {
			recordReader.initialize(new FileSplit(new ClassPathResource("dateVectors.csv").getFile()));
		} catch (IOException | InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

        //Second: the RecordReaderDataSetIterator handles conversion to DataSet objects, ready for use in neural network
        int labelIndex = 5;     //5 values in each row of the iris.txt CSV: 4 input features followed by an integer label (class) index. Labels are the 5th value (index 4) in each row
        int numClasses = 2;     //3 classes (types of iris flowers) in the iris data set. Classes have integer values 0, 1 or 2
        int batchSize = 150;    //Iris data set: 150 examples total. We are loading all of them into one DataSet (not recommended for large data sets)

        DataSetIterator iterator = new RecordReaderDataSetIterator(recordReader,batchSize,labelIndex,numClasses);
        DataSet allData = iterator.next();
        allData.shuffle();
        SplitTestAndTrain testAndTrain = allData.splitTestAndTrain(0.65);  //Use 65% of data for training

        DataSet trainingData = testAndTrain.getTrain();
        DataSet testData = testAndTrain.getTest();

        //We need to normalize our data. We'll use NormalizeStandardize (which gives us mean 0, unit variance):
        DataNormalization normalizer = new NormalizerStandardize();
        normalizer.fit(trainingData);           //Collect the statistics (mean/stdev) from the training data. This does not modify the input data
        normalizer.transform(trainingData);     //Apply normalization to the training data
        normalizer.transform(testData);         //Apply normalization to the test data. This is using statistics calculated from the *training* set
        return new Data(trainingData, testData);
	}

	private INDArray getSubArray(INDArray x, int i){
		INDArray subarray = Nd4j.zeros(1, 5);
		subarray.putScalar(0, 0, x.getDouble(i, 0));
		subarray.putScalar(0, 1, x.getDouble(i, 1));
		subarray.putScalar(0, 2, x.getDouble(i, 2));
		subarray.putScalar(0, 3, x.getDouble(i, 3));
		subarray.putScalar(0, 4, x.getDouble(i, 4));
		return subarray;
	}

	private double[] train(INDArray data, int numIterations, double[] weights, double rate) {
		for (int n=0; n<numIterations; n++) {
			double likelihood = 0.0;
			for (int i=0; i<data.shape()[0]; i++) {
				INDArray x =  this.getSubArray(data,i);
				double predicted = this.classify(x, weights);
				int label = new Double(data.getDouble(i, 4)).intValue();
						for (int j=0; j<data.shape()[1]; j++) {
							weights[j] = weights[j] + rate * (label - predicted) * x.getDouble(0, j);
						}
				// not necessary for learning
						likelihood += label * Math.log(this.classify(x, weights)) + (1-label) 
								* Math.log(1- this.classify(x, weights));
			}
			System.out.println("iteration: " + n + " " + Arrays.toString(weights) + " mle: " + likelihood);
		}
		return weights;
	}
	
	public void performClassification(String vectorsFile){
		double rate = 0.001;
		int numIterations = 3000;
		double[] weights = {0.0,0.0,0.0,0.0,0.0};
		INDArray trainingData = this.preprocessData(vectorsFile).getTrainingData().getFeatureMatrix();
		INDArray testData = this.preprocessData(vectorsFile).getTestData().getFeatureMatrix();
		//Build model
		weights = this.train(trainingData, numIterations, weights, rate);
		
	}

	private double classify(INDArray x, double [] weights) {
		double logit = .0;
		for (int i=0; i<weights.length;i++)  {
			logit += weights[i] * x.getDouble(0, i);
		}
		return 1.0 / (1.0 + Math.exp(logit));
	}

}
