package edu.gwu.seas.csci6907;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Question4 {
	//Read file stored in "src/main/resources" into memory,
	//	assemble in memory via StringBuilder, then return as
	//	String. Method is protected to enable access from
	//	Question6.java
	public String readTextPassageFromFile(String fileLocation){
		BufferedReader br = null;
		StringBuilder sb = new StringBuilder();
		try{
			InputStream is = getClass().getClassLoader()
					.getResourceAsStream(fileLocation);
			br = new BufferedReader(new InputStreamReader(is));
			String line;
			while((line = br.readLine()) != null){
				sb.append(line);
				sb.append(System.lineSeparator());
			}
			//Remove final line break
			sb.setLength(sb.length() - 1);
		} catch(IOException e){
			e.printStackTrace();
		} finally{
			try{
				br.close();
			} catch (IOException e){
				e.printStackTrace();
			}
		}
		return sb.toString();
	}

	//Is this method, we seek to tokenize the passage while preserving certain
	//	characters (e.g.: the "." in abbreviations). Note that substitutions here are equivalent to:
	//	perl -pe 's/[\t\r\n]/ /g;s/([\S])"/$1 "/g;s/"([\S])/" $1/g;s/([\S]),\s/$1 , /g;s/([\S])\.\s/$1 \. /g;s/\\s/\\n/g'
	//	s/([\S])\.\s/$1 \. /g;s/\\s/\\n/g'
	// 	Method is protected to enable access from Question6.java
	protected String performSubstitution(final String passage){
		//Remove tabs, new lines, and carriage returns
		//Corresponding regex: s/[\t\r\n]/ /g
		String tabAndLineBreaksSeparated = passage.replaceAll("[\\t\\r\\n]", " ");
		//Separate ending quotes
		//Corresponding regex: s/([\S])"/$1 "/g
		String endingQuotesSeparated = tabAndLineBreaksSeparated.replaceAll("([\\S])\"", "$1 \"");
		//Separate beginning quotes
		//Corresponding regex: s/"([\S])/" $1/g;
		String beginningQuotesSeparated = endingQuotesSeparated.replaceAll("\"([\\S])", "\" $1");
		//Separate commas
		//Corresponding regex: s/([\S]),\s/$1 , /g;
		String commasSeparated = beginningQuotesSeparated.replaceAll("([\\S]),\\s", "$1 , ");
		//Separate periods
		//Corresponding reges: s/([\S])\.\s/$1 \. /g
		String periodsSeparated = commasSeparated.replaceAll("([\\S])\\.\\s", "$1 \\. ");
		//Separate hyphens
		//Corresponding regex: s/([0-9])-/$1 -/g
		String hyphensSeparated = periodsSeparated.replaceAll("([0-9])-","$1 -");
		//Separate ellipsis
		//Corresponding regex: s/\.\./\. \./g
		String ellipsisSeparated = hyphensSeparated.replaceAll("\\.\\.","\\. \\.");
		//Tokenize: 
		//Corresonding regex: s/\\s/\\n/g'
		String tokenizedString = ellipsisSeparated.replaceAll("\\s", "\n");
		return tokenizedString;
	}

	//Now that we've performed the substitution and replacement, tokenize the string 
	//	by replacing the remaining whitespace characters with spaces.
	private void tokenize(String preprocessedString){
		Pattern regex = Pattern.compile("[\\S]*\n");
		Matcher matcher = regex.matcher(preprocessedString);
		while(matcher.find()){
			System.out.print(matcher.group());
		}
	}

	private void runSubstiution(){
		//Load passage from disk into memory
		final String question4Text = readTextPassageFromFile("question4.txt");
		//Perform substitutions
		final String preprocessedString = performSubstitution(question4Text);
		//Tokenize by whitespaces and print results
		tokenize(preprocessedString);
	}

	public static void main(String [] arghhs){
		new Question4().runSubstiution();
	}

}