package edu.gwu.seas.csci6907;

import java.util.Map;
import java.util.Scanner;
import java.util.TreeMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Question5 {
	//Message displayed to user prompting for a sequence of numbers
	private static final String USER_PROMPT_MESSAGE = "Enter a sequence of integers (or quit to exit): ";
	
	private Map<Integer,Integer> analysisNumberSequence(String userInput){
		Map<Integer,Integer> integerMap = new TreeMap<Integer,Integer>();
		Pattern regex = Pattern.compile("[\\s]?[\\d]+[\\s]?");
		Matcher matcher = regex.matcher(userInput);
		while(matcher.find()){
			int currentInteger = Integer.parseInt(matcher.group().trim());
			if(integerMap.containsKey(currentInteger)){
				integerMap.put(currentInteger, integerMap.get(currentInteger) + 1);
			} else {
				integerMap.put(currentInteger, 1);
			}
		}
		return integerMap;
	}

	//Run program continuously (i.e.: prompting user for input 
	//	and analyzing the provided number series until
	//	user enters "quit"
	private void runNumberSeriesAnalysis(){
		Scanner scanner = new Scanner(System.in);
		System.out.println(USER_PROMPT_MESSAGE);
		String consoleInput;
		while (! (consoleInput = scanner.nextLine()).toLowerCase().equals("quit")) {
			//Remove any trailing or leading white space
			consoleInput = consoleInput.trim();
			//Call analysisNumberSequence() and report results to user
			Map<Integer,Integer> integerMap = analysisNumberSequence(consoleInput);
			int uniqueNumberCount = 0;
			for(Map.Entry<Integer, Integer> entry : integerMap.entrySet()){
				int frequency = entry.getValue();
				uniqueNumberCount += frequency;
				System.out.println(entry.getKey() +" " + frequency);
			} 
			System.out.println("Tokens: " + uniqueNumberCount);
			System.out.println("Types: " + integerMap.size());
			System.out.println(USER_PROMPT_MESSAGE);
		}

		scanner.close();
	}

	public static void main(String [] arghhhs){
		new Question5().runNumberSeriesAnalysis();
	}

}
