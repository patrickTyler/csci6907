package edu.gwu.seas.csci6907;

import java.util.Scanner;

public class Question2 {
	//Message displayed to user prompting for candidate IP address
	private static final String USER_PROMPT_MESSAGE = "Enter a candidate IP address (or quit to exit): ";
	
	//Run regex and determine if String provided via standard input 
	//	matches the specification on an IP address.
	private boolean isValidIpAddress(String candidateIPaddress){
		return candidateIPaddress
				.matches("[0-9][0-9]?[0-9]?\\.[0-9][0-9]?[0-9]?\\.[0-9][0-9]?[0-9]?\\.[0-9][0-9]?[0-9]?");
	}
	
	//Run program continuously (i.e.: prompting user for input 
	//	and validating the provided candidate IP address until 
	//	user enters "quit"
	private void runIPAddressValidation(){
		Scanner scanner = new Scanner(System.in);
		System.out.println(USER_PROMPT_MESSAGE);
		String consoleInput;
        while (! (consoleInput = scanner.nextLine()).toLowerCase().equals("quit")) {
        	//Remove any trailing or leading white space
        	consoleInput = consoleInput.trim();
        	//Call isValidIpAddress() and report results to user
        	String addressValidationSummary = isValidIpAddress(consoleInput) ? "Candidate address: " + consoleInput
        			+ " is a valid IP address." : "Candidate address: " + consoleInput
        			+ " is not a valid IP address.";
        	System.out.println(addressValidationSummary);
        	System.out.println(USER_PROMPT_MESSAGE);
        }

        scanner.close();
	}
	
	public static void main(String [] args){
		new Question2().runIPAddressValidation();
	}
}
