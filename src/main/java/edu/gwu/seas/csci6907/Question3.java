package edu.gwu.seas.csci6907;

import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Question3 {
	//Message displayed to user prompting for input number
	private static final String USER_PROMPT_MESSAGE = "Enter a number (or quit to exit): ";
	//Message displayed to user explaining output of summation
	private static final String USER_OUTPUT_MESSAGE = "Summation of the input is : ";

	//Applies regex, totals digits and returns sum
	private int sumInputString(String inputString){
		//Create regex to extract each digit
		Pattern digitPattern = Pattern.compile("\\d");
		Matcher digitMatcher = digitPattern.matcher(inputString);
		int sum = 0;
		while (digitMatcher.find()) {
			sum += Integer.parseInt(digitMatcher.group());
		}
		return sum;
	}

	//Run program continuously (i.e.: prompting user for input 
	//	and summing total of digits until 
	//	user enters "quit"
	private void runDigitSummation(){
		Scanner scanner = new Scanner(System.in);
		System.out.println(USER_PROMPT_MESSAGE);
		String consoleInput;
		while (! (consoleInput = scanner.nextLine()).toLowerCase().equals("quit")) {
			//Remove any trailing or leading white space
			consoleInput = consoleInput.trim();
			System.out.println(USER_OUTPUT_MESSAGE + sumInputString(consoleInput));
			System.out.println(USER_PROMPT_MESSAGE);
		}

		scanner.close();

	}

	public static void main(String [] args){
		new Question3().runDigitSummation();
	}
}
