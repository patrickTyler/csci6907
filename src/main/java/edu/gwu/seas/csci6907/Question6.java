package edu.gwu.seas.csci6907;

import java.util.Map;
import java.util.TreeMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Question6 extends Question4{
	
	private Map<String,Integer> analysisTextPassage(String preprocessedTextPassage){
		Map<String,Integer> wordFrequencyMap = new TreeMap<String,Integer>();
		Pattern regex = Pattern.compile("[\\s]?[\\S]+[\\s]?");
		Matcher matcher = regex.matcher(preprocessedTextPassage);
		while(matcher.find()){
			String currentWord = matcher.group().trim();
			if(wordFrequencyMap.containsKey(currentWord)){
				wordFrequencyMap.put(currentWord, wordFrequencyMap.get(currentWord) + 1);
			} else {
				wordFrequencyMap.put(currentWord, 1);
			}
		}
		return wordFrequencyMap;
	}
	
	private void runTextPassageAnalysis(){
		final String textPassage = readTextPassageFromFile("question4.txt");
		String preprocessedTextPassage = performSubstitution(textPassage);
		Map<String,Integer> wordFrequencyMap = analysisTextPassage(preprocessedTextPassage);
		int uniqueTokenCount = 0;
		for(Map.Entry<String, Integer> entry : wordFrequencyMap.entrySet()){
			int frequency = entry.getValue();
			uniqueTokenCount += frequency;
			System.out.println(entry.getKey() +" " + frequency);
		}
		System.out.println("Tokens: " + uniqueTokenCount);
		System.out.println("Types: " + wordFrequencyMap.size());
	}
	
	public static void main(String [] arghhss){
		new Question6().runTextPassageAnalysis();
	}

}
