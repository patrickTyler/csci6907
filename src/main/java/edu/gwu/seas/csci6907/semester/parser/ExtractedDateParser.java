package edu.gwu.seas.csci6907.semester.parser;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.google.gson.Gson;

import edu.gwu.seas.csci6907.semester.model.Date;
import edu.gwu.seas.csci6907.semester.model.EntityType;
import edu.gwu.seas.csci6907.semester.model.Date.Entity;
import edu.stanford.nlp.ie.crf.CRFClassifier;
import edu.stanford.nlp.ling.CoreAnnotations;
import edu.stanford.nlp.ling.CoreLabel;
import edu.stanford.nlp.simple.*;

import com.bericotech.clavin.GeoParser;
import com.bericotech.clavin.GeoParserFactory;
import com.bericotech.clavin.resolver.ResolvedLocation;

public class ExtractedDateParser {

	//This particular usage of NER inspired by:
	//	https://blog.openshift.com/
	//	day-14-stanford-ner-how-to-setup-your-own-name-entity-and-recognition-server-in-the-cloud/

	private CRFClassifier<CoreLabel> instantiateClassifier(String classifierFileLocation){
		String serializedClassifier = classifierFileLocation;
		CRFClassifier<CoreLabel> classifier = 
				CRFClassifier.getClassifierNoExceptions(serializedClassifier);
		return classifier;
	}

	private List<Entity> performNER(String text, CRFClassifier<CoreLabel> classifier) {
		List<List<CoreLabel>> classify = classifier.classify(text);
		List<Entity> entities = new ArrayList<>();
		for (List<CoreLabel> coreLabels : classify) {
			for (CoreLabel coreLabel : coreLabels) {
				String word = coreLabel.word();
				String answer = coreLabel.get(CoreAnnotations.AnswerAnnotation.class);
				if(!"O".equals(answer)){
					if(answer.equals("ORGANIZATION")){
						entities.add(new Entity(word, EntityType.ORGANIZATION));
					} else if(answer.equals("PERSON")){
						entities.add(new Entity(word, EntityType.PERSON));
					}

				}
			}
		}
		return entities;
	}

	private List<ResolvedLocation> getLocations(String locationInput, String indexDirectoryPath){
		GeoParser parser;
		List<ResolvedLocation> resolvedLocation = new ArrayList<>();
		StringBuilder sb = new StringBuilder();
		try {
			// Instantiate the CLAVIN GeoParser
			parser = GeoParserFactory.getDefault(indexDirectoryPath);
			// Parse location names in the text into geographic entities
			for(ResolvedLocation location : parser.parse(locationInput)){
				resolvedLocation.add(location);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return resolvedLocation;
	}

	private Map<String,List<Date>> extractParsedDates(String extractedDatesFile){
		BufferedReader reader = null;
		Map<String,List<Date>> dateMap = new HashMap<>();
		Gson gson = new Gson();
		try {
			reader = new BufferedReader(new FileReader(extractedDatesFile));
			String line;
			boolean newFile = true;
			boolean waitingForBlankLine = false;
			String sourceFileName = null;
			StringBuilder sb = new StringBuilder();
			int lineCounter = 0;
			while((line = reader.readLine()) != null) {
				//Handle the case for the start of a new file,
				//	e.g.: "/home/ktyler/Documents/strider/NYT/txt/10042_text.txt"
				if(newFile){
					sourceFileName = line.trim();
					newFile = false;
				} else if(line.contains("]")){
					if(line.contains("[")){
						dateMap.put(sourceFileName, null);
						sourceFileName = null;
						waitingForBlankLine = true;
					} else {
						sb.append(']');
						waitingForBlankLine = true;
						//Parse new Date object
						JSONArray jsonarray = new JSONArray(sb.toString());
						List<Date> dates = new ArrayList<>();
						for (int i = 0; i < jsonarray.length(); i++) {
							Date date = null;
							JSONObject rawDateObject = jsonarray.getJSONObject(i);
							try{
								if(rawDateObject.getJSONObject("value") != null){
									if(rawDateObject.getJSONObject("value").has("begin") &&
											rawDateObject.getJSONObject("value").has("end")){
										String beginning = rawDateObject.getJSONObject("value").getString("begin");
										String ending = rawDateObject.getJSONObject("value").getString("end");
										date = new Date(rawDateObject.getInt("start"), rawDateObject.getInt("end"), 
												rawDateObject.getString("text"), rawDateObject.getString("type"), 
												beginning + ";" + ending);
									} else {
										if(rawDateObject.getJSONObject("value").has("begin")){
											date = new Date(rawDateObject.getInt("start"), rawDateObject.getInt("end"), 
													rawDateObject.getString("text"), rawDateObject.getString("type"), 
													rawDateObject.getJSONObject("value").getString("begin"));
										}
										else {
											date = new Date(rawDateObject.getInt("start"), rawDateObject.getInt("end"), 
													rawDateObject.getString("text"), rawDateObject.getString("type"), 
													"");
										}
									}
								}
							} catch(JSONException e){
								date = gson.fromJson(
										jsonarray.getJSONObject(i).toString(), Date.class);
							}
							dates.add(date);
						}
						dateMap.put(sourceFileName, dates);
						sb.setLength(0);
						sourceFileName = null;
					}
				} else if(waitingForBlankLine){
					waitingForBlankLine = false;
					newFile = true;
				} else{
					sb.append(line);
				}
				if(lineCounter == 1000){
					System.exit(0);
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				reader.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return dateMap;
	}

	private int findWordOccurence(String articleText, String substring, int numOccurence){
		int numSubstringsFound = 1;
		int fromIndex = 0;
		int substringIndex = 0;
		while(numSubstringsFound <= numOccurence){
			substringIndex = articleText.indexOf(substring, fromIndex);
			numSubstringsFound++;
		}
		return substringIndex;
	}

	private StanfordSentence buildContextWindow(List<Sentence>  articleText, String substring, int numOccurences){
		int numOccurencesFound = 0;
		String sentence = null;
		Sentence stanfordSentence = null;
		for (Sentence sent : articleText) {  
			stanfordSentence = sent;
			sentence = sent.text();
			numOccurencesFound += StringUtils.countMatches(sentence, substring);
			if(numOccurencesFound == numOccurences){
				break;
			}
		}
		//	such that we can split our window size in half
		return new StanfordSentence(sentence, stanfordSentence);
	}

	private  Map<String,List<Date>> appendContextWindowToDates( Map<String,List<Date>> dateMap, 
			String clavinIndexDirectoryPath, CRFClassifier<CoreLabel> classifier){
		Map<String,List<Date>> updatedMap = new HashMap<>();
		for (Map.Entry<String, List<Date>> entry : dateMap.entrySet()) {
			List<Date> updatedDates = new ArrayList<>();
			BufferedReader reader = null;
			try{
				reader = new BufferedReader(new FileReader(entry.getKey()));
				String line;
				StringBuilder sb = new StringBuilder();
				while((line = reader.readLine()) != null) {
					sb.append(line);
				}
				//For whatever reason, some our test data has been tokenized incorrectly.
				//Here we (re-)add the space between sentences to ensure that tokenization
				//and sentence parsing occurs correctly.
				String articleText = sb.toString().replaceAll("\\.(\\S)", "\\. $1");
				List<Sentence> annotatedArticle = new Document(articleText).sentences();
				List<Date> dates = entry.getValue();
				Map<String,Integer> occurenceMap = new HashMap<>();
				for(Date date : dates){
					String dateText = date.getText();
					int numOccurences = occurenceMap.containsKey(dateText) ? 
							occurenceMap.get(dateText) + 1 : 1;
					occurenceMap.put(dateText, numOccurences);
					//Set the context window (i.e.: the sentence in which the token
					//	originally appears.
					StanfordSentence sentence = buildContextWindow(annotatedArticle, dateText, numOccurences);
					date.setSentence(sentence.getSentence());
					date.setStanfordSentence(sentence.getStanfordSentence());
					//Set the locations
					date.setLocations(getLocations(sentence.getSentence(), clavinIndexDirectoryPath));
					//Set entities
					date.setEntities(performNER(sentence.getSentence(),classifier));
					updatedDates.add(date);
				}
				updatedMap.put(entry.getKey(), updatedDates);
			} catch (IOException e) {
				e.printStackTrace();
			} finally {
				try {
					reader.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		return updatedMap;
	}
	
	public Map<String,List<Date>> parseDatesFromText(String extractedDatesFile, String clavinIndexDirectory, String classifierFile){
		CRFClassifier<CoreLabel> classifier = instantiateClassifier(classifierFile);
		Map<String,List<Date>> dateMap = extractParsedDates(extractedDatesFile);
		dateMap = appendContextWindowToDates(dateMap, clavinIndexDirectory, classifier);
		return dateMap;
	}

	static class StanfordSentence{
		private String sentence;
		private Sentence stanfordSentence;
		public StanfordSentence(String sentence, Sentence stanfordSentence){
			this.setSentence(sentence);
			this.setStanfordSentence(stanfordSentence);
		}
		public Sentence getStanfordSentence() {
			return stanfordSentence;
		}
		public void setStanfordSentence(Sentence stanfordSentence) {
			this.stanfordSentence = stanfordSentence;
		}
		public String getSentence() {
			return sentence;
		}
		public void setSentence(String sentence) {
			this.sentence = sentence;
		}
	}
}
