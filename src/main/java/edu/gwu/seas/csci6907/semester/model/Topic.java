package edu.gwu.seas.csci6907.semester.model;

import java.util.HashSet;
import java.util.Set;

public class Topic {
	private Set<String> associatedTerms;
	public Topic(Set<String> associatedTerms){
		this.associatedTerms = (associatedTerms);
	}
	
	public Topic(String associatedTerms){
		this.associatedTerms = new HashSet<>();
		for(String term : associatedTerms.split(" ")){
			this.associatedTerms.add(term);
		}
	}
	
	public Set<String> getAssociatedTerms() {
		return associatedTerms;
	}
}
