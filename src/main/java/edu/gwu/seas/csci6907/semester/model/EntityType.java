package edu.gwu.seas.csci6907.semester.model;

public enum EntityType {
	PERSON,
	ORGANIZATION
}
