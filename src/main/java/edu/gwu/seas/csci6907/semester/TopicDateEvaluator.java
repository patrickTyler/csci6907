package edu.gwu.seas.csci6907.semester;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Set;

import com.bericotech.clavin.GeoParser;
import com.bericotech.clavin.GeoParserFactory;
import com.bericotech.clavin.gazetteer.query.FuzzyMode;
import com.bericotech.clavin.gazetteer.query.LuceneGazetteer;
import com.bericotech.clavin.gazetteer.query.QueryBuilder;
import com.bericotech.clavin.resolver.ResolvedLocation;

import edu.gwu.seas.csci6907.semester.model.Date;
import edu.gwu.seas.csci6907.semester.model.Date.Entity;
import edu.gwu.seas.csci6907.semester.model.Topic;
import edu.stanford.nlp.ling.CoreLabel;
import edu.stanford.nlp.ling.CoreAnnotations.PartOfSpeechAnnotation;
import edu.stanford.nlp.ling.CoreAnnotations.TokensAnnotation;
import edu.stanford.nlp.simple.Document;
import edu.stanford.nlp.simple.Sentence;
import edu.stanford.nlp.util.CoreMap;

import org.deeplearning4j.models.word2vec.Word2Vec;
import org.nd4j.linalg.api.ndarray.INDArray;
import org.nd4j.linalg.factory.Nd4j;

public class TopicDateEvaluator {
	private static final double meterRadius = 80000.0;

	//Estiamte path distance from trigger word to date
	private static int estimatePathDistance(Date date){
		// these are all the sentences in this document
		// a CoreMap is essentially a Map that uses class objects as keys and has values with custom types
		List<Sentence> document =  new Document(date.getSentence()).sentences();
		int verbNumber = 0;
		int dateNumber = 0;
		int dPathSize = 0;
		for(Sentence s : document){
			CoreMap sentence = s.asCoreMap(Sentence::posTags, Sentence::parse);
			for (CoreLabel token: sentence.get(TokensAnnotation.class)) {
				// this is the POS tag of the token
				String pos = token.get(PartOfSpeechAnnotation.class);
				if(pos.contains("VBD")){
					verbNumber = token.index() -1;
				} 
				if(token.originalText().contains(
						date.getText())
						|| date.getText().contains(token.originalText())){
					dateNumber = token.index() -1;
				}
			}
			List<String> dependencyPath = s.algorithms().dependencyPathBetween(dateNumber, verbNumber);
			for(String pathItem : dependencyPath){
				if(pathItem.length() > 0 && ! pathItem.contains(">") && ! pathItem.contains("<")){
					dPathSize++;
				}
			}
		}
		return dPathSize;
	}

	/**
	 * This function calculates the distance between two geographic places, taking into consideration the 
	 * latitude, longitude, and elevation of both locations, while accounting for the curvature of the Earth. 
	 * 
	 * Taken from: 
	 * https://stackoverflow.com/questions/3694380/calculating-distance-between-two-points-using-latitude-longitude-what-am-i-doi
	 * 
	 * Calculate distance between two points in latitude and longitude taking
	 * into account height difference. If you are not interested in height
	 * difference pass 0.0. Uses Haversine method as its base.
	 * 
	 * lat1, lon1 Start point lat2, lon2 End point el1 Start altitude in meters
	 * el2 End altitude in meters
	 * @returns Distance in Meters
	 */
	private static double haversineDistance(double lat1, double lat2, double lon1,
			double lon2, double el1, double el2) {

		final int R = 6371; // Radius of the earth

		double latDistance = Math.toRadians(lat2 - lat1);
		double lonDistance = Math.toRadians(lon2 - lon1);
		double a = Math.sin(latDistance / 2) * Math.sin(latDistance / 2)
				+ Math.cos(Math.toRadians(lat1)) * Math.cos(Math.toRadians(lat2))
				* Math.sin(lonDistance / 2) * Math.sin(lonDistance / 2);
		double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
		double distance = R * c * 1000; // convert to meters

		double height = el1 - el2;

		distance = Math.pow(distance, 2) + Math.pow(height, 2);

		return Math.sqrt(distance);
	}

	public static DateEvaluation estimateConfidence(Topic topic, Date date, String indexDirectoryPath, Word2Vec word2vec,
			Set<String> stopwords, Set<String> commonFalsePositives, double [] weightVector){
		boolean sufficientNumberOfLocations = false;
		boolean sufficientNumberOfEntities = false;
		boolean isWithinAcceptablePath = false;
		boolean containsCommonFalsePositiveWords = false;
		double locationalSimiliariy = 0.0;
		double entitySimilarity = 0.0;
		double embeddingSimilarity = 0.0;
		//Person and organizational entity similarity
		List<Entity> dateEntities = date.getEntities();
		Set<String> topicWords = topic.getAssociatedTerms();
		double numDateEntities = dateEntities.size() * 1.0;
		int numOverlappingEntities = 0;
		if(numDateEntities > 0){
			sufficientNumberOfEntities = true;
			for(Entity dateEntity : dateEntities){
				String dateName = dateEntity.getName().toLowerCase();
				for(String topicWord : topicWords){
					if(topicWord.toLowerCase().contains(dateName)
							|| dateName.contains(topicWord)){
						numOverlappingEntities++;
						break;
					}
				}
			}
			entitySimilarity = numOverlappingEntities / numDateEntities;
		}


		//Location entity simliarity
		// Instantiate the CLAVIN GeoParser
		GeoParser parser;
		LuceneGazetteer instance;
		QueryBuilder queryBuilder;
		List<ResolvedLocation> topicLocations = new ArrayList<>();
		List<ResolvedLocation> dateLocations = date.getLocations();
		try{
			parser = GeoParserFactory.getDefault(indexDirectoryPath);
			instance = new LuceneGazetteer(new File(indexDirectoryPath));
			queryBuilder = new QueryBuilder().maxResults(1).fuzzyMode(FuzzyMode.NO_EXACT);
			topicWords = topic.getAssociatedTerms();
			for(String topicWord : topicWords){
				for(ResolvedLocation location : 
					instance.getClosestLocations(queryBuilder.location(topicWord).build())){
					if(! location.isFuzzy()){
						topicLocations.add(location);
					}
				}
			}

		} catch(Exception e){
			e.printStackTrace();
		}
		double numDateLocations = dateLocations.size() * 1.0;
		System.out.println("Num topic locations: " + topicLocations.size() + ", numDates: " + numDateLocations);
		if(numDateLocations > 0 && topicLocations.size() > 0){
			sufficientNumberOfLocations = true;
			//Loop through each location found in the date's context window.
			//For each location- is this location <= 50 miles (80,000 m) of
			//a location associated with the topic?
			int numCooccuringLocations = 0;
			for(ResolvedLocation dateLocation : dateLocations){
				double dateLat = dateLocation.getGeoname().getLatitude();
				double dateLon = dateLocation.getGeoname().getLongitude();
				double dateEle = dateLocation.getGeoname().getElevation();
				for(ResolvedLocation topicLocation : topicLocations){
					double topicLat = topicLocation.getGeoname().getLatitude();
					double topicLon = topicLocation.getGeoname().getLongitude();
					double topicEle = topicLocation.getGeoname().getElevation();
					if(haversineDistance(dateLat, topicLat, dateLon,
							topicLon, dateEle, topicEle) <= TopicDateEvaluator.meterRadius){
						numCooccuringLocations++;
						break;
					}
				}
			}
			locationalSimiliariy = numCooccuringLocations / numDateLocations;
		}


		//Word embedding similarity
		//Here we wish to take the sentence in which the date appears along with the 
		//the words associated with our topic, and examine the word embedding similarity
		//of each. To do so, we use the "getWordVectorsMean" convenience method of 
		//Word2Vec, which returns the "mean vector" of a collection of words. We
		//then calculate the L2 distance between the two vectors, then normalize
		//to obtain a similarity score.
		List<String> topicWordsCopy  = new ArrayList<>();
		//Remove any words that are not within the Word2Vec vocab, since this would
		//	negatively effect the mean vector.
		for(String topicWord : topic.getAssociatedTerms()){
			if(word2vec.hasWord(topicWord) && ! stopwords.contains(topicWord)){
				topicWordsCopy.add(topicWord);
			}
		}
		List<String> dateWords = new ArrayList<>();
		for(String dateWord : date.getSentence().split(" ")){
			if(word2vec.hasWord(dateWord) && ! stopwords.contains(dateWord)){
				dateWords.add(dateWord);
			}
		}

		INDArray topicMeanVector = word2vec.getWordVectorsMean(topicWordsCopy);
		INDArray dateContextWindowMeanVector = 
				word2vec.getWordVectorsMean(dateWords);
		double denominator = 
				(dateContextWindowMeanVector.norm2Number().doubleValue() * topicMeanVector.norm2Number().doubleValue());
		double dotProduct = 0.0;
		for(int i = 0; i < word2vec.getLayerSize(); i++){
			dotProduct += (topicMeanVector.getDouble(i) * dateContextWindowMeanVector.getDouble(i));
		}
		embeddingSimilarity = dotProduct / denominator;


		//Date parsing
		int estimatedPathDistance = estimatePathDistance(date);
		isWithinAcceptablePath = (estimatedPathDistance <= 7) && (estimatedPathDistance > 0);

		//Verify that none of the words typically associated with false positives are
		//	present in the context window.
		for(String token : date.getSentence().split(" ")){
			if(commonFalsePositives.contains(token.toLowerCase())){
				containsCommonFalsePositiveWords = true;
				break;
			}
		}

		//Perform weighted sum
		/*
		boolean containsCommonFalsePositiveWords = false;
		double embeddingSimilarity = 0.0; */
		double totalPossibleWeight = 0.0;
		double totalScore = 0.0;
		//Locational similiarity
		if(sufficientNumberOfLocations){
			totalPossibleWeight += weightVector[0];
			totalScore = (weightVector[0] * locationalSimiliariy);
		}
		//Entity similarity
		if(sufficientNumberOfEntities){
			totalPossibleWeight += weightVector[1];
			totalScore += (entitySimilarity * weightVector[1]);
		}
		//Path similarity
		totalPossibleWeight += weightVector[2];
		if(isWithinAcceptablePath){
			totalScore += weightVector[2];
		}
		//Embedding similarity
		totalPossibleWeight += weightVector[3];
		totalScore += (embeddingSimilarity * weightVector[3]);

		//Common false positive words score
		totalPossibleWeight += weightVector[4];
		if(! containsCommonFalsePositiveWords){
			totalScore += (weightVector[4]);
		}
		
		//Build confidence vector
		INDArray confidenceVector = Nd4j.zeros(1, 5);
		confidenceVector.putScalar(0, 0, locationalSimiliariy);
		confidenceVector.putScalar(0, 1, entitySimilarity);
		confidenceVector.putScalar(0, 2, embeddingSimilarity);
		if(isWithinAcceptablePath){
			confidenceVector.putScalar(0, 3, 1);
		} else{
			confidenceVector.putScalar(0, 3, 0);
		}
		if(containsCommonFalsePositiveWords){
			confidenceVector.putScalar(0, 4, 0);
		} else{
			confidenceVector.putScalar(0, 4, 1);
		}
		
		return new DateEvaluation((1.0 * totalScore) / totalPossibleWeight, confidenceVector);
	}
	
	static class DateEvaluation{
		private double confidenceScore;
		private INDArray confidenceVector;
		public DateEvaluation(double confidenceScore, INDArray confidenceVector){
			this.confidenceScore = confidenceScore;
			this.confidenceVector = confidenceVector;
		}
		public double getConfidenceScore() {
			return confidenceScore;
		}
		public INDArray getConfidenceVector() {
			return confidenceVector;
		}
	}
}
