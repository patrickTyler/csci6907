package edu.gwu.seas.csci6907.semester.parser;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import edu.gwu.seas.csci6907.semester.model.Topic;

public class ExtractedTopicParser {
	
	public Set<String> buildStopList(String stoplistFile){
		BufferedReader reader = null;
		Set<String> stopwords = new HashSet<>();
		try{
			reader = new BufferedReader(new FileReader(stoplistFile));
			String line;
			while((line = reader.readLine()) != null) {
				stopwords.add(line.trim());
			}
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				reader.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return stopwords;
	}
	
	public List<Topic> parseTopics(String extractedTopicsFile, Set<String> stoplist){
		BufferedReader reader = null;
		String line = "";
		List<Topic> topics = new ArrayList<>();
		try{
			reader = new BufferedReader(new FileReader(extractedTopicsFile));
			while((line = reader.readLine()) != null) {
				if(line.length() > 1 ){
					Set<String> topicWords = new HashSet<>();
					String [] delimitedLine = line.split("\t")[2].split(" ");
					for(String token : delimitedLine){
						//Some tokens have been incorrectly parse,
						//	such that they contain an erroneous period.
						//	Remove this period (thereby splitting the token
						//	in two), and add the two new tokens to the list
						//	provided they are non-stop words.
							if(! token.contains(".")){
								topicWords.add(token);
							} else{
								String [] periodDelimitedToken = token.split("\\.");
								if(! stoplist.contains(periodDelimitedToken[0])){
									topicWords.add(periodDelimitedToken[0]);
								}
								if(! stoplist.contains(periodDelimitedToken[1])){
									topicWords.add(periodDelimitedToken[1]);
								}
							}
					}
					topics.add(new Topic(topicWords));
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				reader.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return topics;
	}
	
	
	public static void main(String [] arggghs){
		Set<String> stoplist =  new ExtractedTopicParser().buildStopList("/home/ktyler/git/Mallet/stoplists/en.txt");
		new ExtractedTopicParser().parseTopics(
				"/home/ktyler/Documents/strider/events/nyt_topic_analysis/6000topics.txt", stoplist);
	}

}
