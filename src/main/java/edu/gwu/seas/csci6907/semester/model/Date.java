package edu.gwu.seas.csci6907.semester.model;

import java.util.List;

import com.bericotech.clavin.resolver.ResolvedLocation;

import edu.stanford.nlp.simple.Sentence;

//Convenience class used for GSON deserialization.
//Modeled after JSON data type from the SUTime package
	//"end": 750, 
  //"start": 742, 
  //"text": "daylight", 
  //"type": "TIME", 
  //"value": "2017-12-01TDT"

public class Date{
	private int end;
	private int start;
	private String text;
	private String type;
	private String value;
	private String sentence;
	private Sentence stanfordSentence;
	private List<ResolvedLocation> locations;
	private List<Entity> entities;
	public Date(int start, int end, String text, String type, String value){
		this.end = end;
		this.start = start;
		this.text = text;
		this.type = type;
		this.value = value;
	}
	public int getEnd(){
		return this.end;
	}
	public int getStart(){
		return this.start;
	}
	public String getText(){
		return this.text;
	}
	public String getType(){
		return this.type;
	}
	public String getValue(){
		return this.value;
	}
	public Sentence getStanfordSentence() {
		return stanfordSentence;
	}
	public void setStanfordSentence(Sentence stanfordSentence) {
		this.stanfordSentence = stanfordSentence;
	}
	public String getSentence() {
		return sentence;
	}
	public void setSentence(String sentence) {
		this.sentence = sentence;
	}
	public List<ResolvedLocation> getLocations() {
		return locations;
	}
	public void setLocations(List<ResolvedLocation> locations) {
		this.locations = locations;
	}
	
	public List<Entity> getEntities() {
		return entities;
	}
	public void setEntities(List<Entity> entities) {
		this.entities = entities;
	}

	public static class Entity{
		private String name;
		private EntityType type;
		public Entity(String name, EntityType type){
			this.setName(name);
			this.setType(type);
		}
		public EntityType getType() {
			return type;
		}
		public void setType(EntityType type) {
			this.type = type;
		}
		public String getName() {
			return name;
		}
		public void setName(String name) {
			this.name = name;
		}
	}

}
